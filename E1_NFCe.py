import ctypes
import platform

if platform.system() == "Windows":
  ffi = ctypes.WinDLL("./E1_NFCe02.dll")
else:
  ffi = ctypes.cdll.LoadLibrary("./libE1_NFCe02.so")


def AbreCupomVenda(caminho_arquivo, UF, cNF, natOp, mod, serie, nNF, dhEmi, dhSaiEnt, tpNF, idDest, cMunFG, tpImp, tpEmis, cDV, 
tpAmb, finNFe, indFinal, indPres, indIntermed, procEmi, verProc, dhCont, xJust, idLocal):
    
    fn = ffi.AbreCupomVenda

    fn.restype = ctypes.c_int

    fn.argtypes = [ctypes.c_char_p, 
    ctypes.c_char_p, 
    ctypes.c_char_p, 
    ctypes.c_char_p, 
    ctypes.c_int, 
    ctypes.c_char_p, 
    ctypes.c_char_p, 
    ctypes.c_char_p,
    ctypes.c_char_p, 
    ctypes.c_int, 
    ctypes.c_int, 
    ctypes.c_char_p, 
    ctypes.c_int, 
    ctypes.c_int, 
    ctypes.c_int, 
    ctypes.c_int, 
    ctypes.c_int, 
    ctypes.c_int,
    ctypes.c_int, 
    ctypes.c_int, 
    ctypes.c_int, 
    ctypes.c_char_p, 
    ctypes.c_char_p, 
    ctypes.c_char_p, 
    ctypes.c_char_p]

    caminho_arquivo = ctypes.c_char_p(bytes(caminho_arquivo, "utf-8"))
    UF              = ctypes.c_char_p(bytes(UF,              "utf-8"))
    cNF             = ctypes.c_char_p(bytes(cNF,             "utf-8"))
    natOp           = ctypes.c_char_p(bytes(natOp,           "utf-8"))
    serie           = ctypes.c_char_p(bytes(serie,           "utf-8"))
    nNF             = ctypes.c_char_p(bytes(nNF,             "utf-8"))
    dhEmi           = ctypes.c_char_p(bytes(dhEmi,           "utf-8"))
    dhSaiEnt        = ctypes.c_char_p(bytes(dhSaiEnt,        "utf-8"))
    cMunFG          = ctypes.c_char_p(bytes(cMunFG,          "utf-8"))
    verProc         = ctypes.c_char_p(bytes(verProc,         "utf-8"))
    dhCont          = ctypes.c_char_p(bytes(dhCont,          "utf-8"))
    xJust           = ctypes.c_char_p(bytes(xJust,           "utf-8"))
    idLocal         = ctypes.c_char_p(bytes(idLocal,         "utf-8"))
    
    return fn(caminho_arquivo, UF, cNF, natOp, mod, serie, nNF, dhEmi, dhSaiEnt, tpNF, idDest, cMunFG, tpImp, tpEmis, cDV, tpAmb, finNFe, indFinal, indPres,
    indIntermed, procEmi, verProc, dhCont, xJust, idLocal)

def InformaEmitente(caminho_arquivo, CNPJ, CPF, xNome, xFant, xLgr, nro, xCpl, xBairro, cMun, xMun, UF, CEP, cPais, xPais, fone, IE, IEST, IM, CNAE, CRT):
    
    fn = ffi.InformaEmitente

    fn.restype = ctypes.c_int

    fn.argtypes = [ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_int,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_int,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_int]

    caminho_arquivo = ctypes.c_char_p(bytes(caminho_arquivo, "utf-8"))
    CNPJ            = ctypes.c_char_p(bytes(CNPJ,            "utf-8"))
    CPF             = ctypes.c_char_p(bytes(CPF,             "utf-8"))
    xNome           = ctypes.c_char_p(bytes(xNome,           "utf-8"))
    xFant           = ctypes.c_char_p(bytes(xFant,           "utf-8"))
    xLgr            = ctypes.c_char_p(bytes(xLgr,            "utf-8"))
    nro             = ctypes.c_char_p(bytes(nro,             "utf-8"))
    xCpl            = ctypes.c_char_p(bytes(xCpl,            "utf-8"))
    xBairro         = ctypes.c_char_p(bytes(xBairro,         "utf-8"))
    xMun            = ctypes.c_char_p(bytes(xMun,            "utf-8"))
    UF              = ctypes.c_char_p(bytes(UF,              "utf-8"))
    CEP             = ctypes.c_char_p(bytes(CEP,             "utf-8"))
    xPais           = ctypes.c_char_p(bytes(xPais,           "utf-8"))
    fone            = ctypes.c_char_p(bytes(fone,            "utf-8"))
    IE              = ctypes.c_char_p(bytes(IE,              "utf-8"))
    IEST            = ctypes.c_char_p(bytes(IEST,            "utf-8"))
    IM              = ctypes.c_char_p(bytes(IM,              "utf-8"))
    CNAE            = ctypes.c_char_p(bytes(CNAE,            "utf-8"))

    return fn(caminho_arquivo, CNPJ, CPF, xNome, xFant, xLgr, nro, xCpl, xBairro, cMun, xMun, UF, CEP, cPais, xPais, fone, IE, IEST, IM, CNAE, CRT)

def InformaProduto(caminho_arquivo, cProd, cEAN, xProd, NCM, NVE, CEST, indEscala, CNPJFab, cBenef, EXTIPI, CFOP, uCom, qCom, vUnCom, vProd, cEANTrib,
uTrib, qTrib, vUnTrib, vFrete, vSeg, vDesc, vOutro, indTot):

    fn = ffi.InformaProduto

    fn.restype = ctypes.c_int

    fn.argtypes = [ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_int]

    caminho_arquivo = ctypes.c_char_p(bytes(caminho_arquivo, "utf-8"))
    cProd           = ctypes.c_char_p(bytes(cProd          , "utf-8"))    
    cEAN            = ctypes.c_char_p(bytes(cEAN           , "utf-8"))    
    xProd           = ctypes.c_char_p(bytes(xProd          , "utf-8"))
    NCM             = ctypes.c_char_p(bytes(NCM            , "utf-8"))
    NVE             = ctypes.c_char_p(bytes(NVE            , "utf-8"))
    CEST            = ctypes.c_char_p(bytes(CEST           , "utf-8"))    
    indEscala       = ctypes.c_char_p(bytes(indEscala      , "utf-8"))    
    CNPJFab         = ctypes.c_char_p(bytes(CNPJFab        , "utf-8"))
    cBenef          = ctypes.c_char_p(bytes(cBenef         , "utf-8"))
    EXTIPI          = ctypes.c_char_p(bytes(EXTIPI         , "utf-8"))
    CFOP            = ctypes.c_char_p(bytes(CFOP           , "utf-8"))
    uCom            = ctypes.c_char_p(bytes(uCom           , "utf-8"))
    qCom            = ctypes.c_char_p(bytes(qCom           , "utf-8"))
    vUnCom          = ctypes.c_char_p(bytes(vUnCom         , "utf-8"))
    vProd           = ctypes.c_char_p(bytes(vProd          , "utf-8"))
    cEANTrib        = ctypes.c_char_p(bytes(cEANTrib       , "utf-8"))
    uTrib           = ctypes.c_char_p(bytes(uTrib          , "utf-8"))
    qTrib           = ctypes.c_char_p(bytes(qTrib          , "utf-8"))
    vUnTrib         = ctypes.c_char_p(bytes(vUnTrib        , "utf-8"))    
    vFrete          = ctypes.c_char_p(bytes(vFrete         , "utf-8"))
    vSeg            = ctypes.c_char_p(bytes(vSeg           , "utf-8"))
    vDesc           = ctypes.c_char_p(bytes(vDesc          , "utf-8"))
    vOutro          = ctypes.c_char_p(bytes(vOutro         , "utf-8"))

    return fn(caminho_arquivo, cProd, cEAN, xProd, NCM, NVE, CEST, indEscala, CNPJFab, cBenef, EXTIPI, CFOP, uCom, qCom, vUnCom, vProd, cEANTrib, 
    uTrib, qTrib, vUnTrib, vFrete, vSeg, vDesc, vOutro, indTot)

def InformaICMS40(caminho_arquivo, nItem, orig, CST, vICMSDeson, motDesICMS):

    fn = ffi.InformaICMS40

    fn.restype = ctypes.c_int

    fn.argtypes = [ctypes.c_char_p,
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p]

    caminho_arquivo = ctypes.c_char_p(bytes(caminho_arquivo, "utf-8"))
    CST             = ctypes.c_char_p(bytes(CST,             "utf-8"))
    vICMSDeson      = ctypes.c_char_p(bytes(vICMSDeson,      "utf-8"))
    motDesICMS      = ctypes.c_char_p(bytes(motDesICMS,      "utf-8"))
    
    return fn(caminho_arquivo, nItem, orig, CST, vICMSDeson, motDesICMS)

def InformaPISAliq(caminho_arquivo, nItem, CST, vBC, pPIS, vPIS):

    fn = ffi.InformaPISAliq

    fn.restype = ctypes.c_int

    fn.argtypes = [ctypes.c_char_p,
    ctypes.c_int,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p]

    caminho_arquivo = ctypes.c_char_p(bytes(caminho_arquivo, "utf-8"))
    CST             = ctypes.c_char_p(bytes(CST,             "utf-8"))
    vBC             = ctypes.c_char_p(bytes(vBC,             "utf-8"))
    pPIS            = ctypes.c_char_p(bytes(pPIS,            "utf-8"))
    vPIS            = ctypes.c_char_p(bytes(vPIS,            "utf-8"))

    return fn(caminho_arquivo, nItem, CST, vBC, pPIS, vPIS)

def InformaCofinsAliq(caminho_arquivo, nItem, CST, vBC, pCOFINS, vCOFINS):

    fn = ffi.InformaCofinsAliq

    fn.restype = ctypes.c_int

    fn.argtypes = [ctypes.c_char_p,
    ctypes.c_int,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p]

    caminho_arquivo = ctypes.c_char_p(bytes(caminho_arquivo, "utf-8"))
    CST             = ctypes.c_char_p(bytes(CST,             "utf-8"))
    vBC             = ctypes.c_char_p(bytes(vBC,             "utf-8"))
    pCOFINS         = ctypes.c_char_p(bytes(pCOFINS,         "utf-8"))
    vCOFINS         = ctypes.c_char_p(bytes(vCOFINS,         "utf-8"))

    return fn(caminho_arquivo, nItem, CST, vBC, pCOFINS, vCOFINS)

def InformaPagamento(caminho_arquivo, indPag, tPag, vPag, tpIntegra, CNPJ, tBand, cAut, vTroco):
    
    fn = ffi.InformaPagamento

    fn.restype = ctypes.c_int

    fn.argtypes = [ctypes.c_char_p,
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_char_p,
    ctypes.c_int,
    ctypes.c_char_p,
    ctypes.c_int,
    ctypes.c_char_p,
    ctypes.c_char_p]

    caminho_arquivo = ctypes.c_char_p(bytes(caminho_arquivo, "utf-8"))
    vPag            = ctypes.c_char_p(bytes(vPag,            "utf-8"))
    CNPJ            = ctypes.c_char_p(bytes(CNPJ,            "utf-8"))
    cAut            = ctypes.c_char_p(bytes(cAut,            "utf-8"))
    vTroco          = ctypes.c_char_p(bytes(vTroco,          "utf-8"))

    return fn(caminho_arquivo, indPag, tPag, vPag, tpIntegra, CNPJ, tBand, cAut, vTroco)

def InformaTransporte(caminho_arquivo, modFrete, CNPJ, CPF, xNome, IE, xEnder, xMun, UF):
    
    fn = ffi.InformaTransporte

    fn.restype = ctypes.c_int

    fn.argtypes = [ctypes.c_char_p,
    ctypes.c_int,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p]

    caminho_arquivo = ctypes.c_char_p(bytes(caminho_arquivo, "utf-8"))
    CNPJ            = ctypes.c_char_p(bytes(CNPJ,            "utf-8"))    
    CPF             = ctypes.c_char_p(bytes(CPF,             "utf-8"))
    xNome           = ctypes.c_char_p(bytes(xNome,           "utf-8"))
    IE              = ctypes.c_char_p(bytes(IE,              "utf-8"))
    xEnder          = ctypes.c_char_p(bytes(xEnder,          "utf-8"))
    xMun            = ctypes.c_char_p(bytes(xMun,            "utf-8"))
    UF              = ctypes.c_char_p(bytes(UF,              "utf-8"))


    return fn(caminho_arquivo, modFrete, CNPJ, CPF, xNome, IE, xEnder, xMun, UF)

def InformaVolumes(caminho_arquivo, qVol, especie, marca, nVol, pesoL, pesoB):

    fn = ffi.InformaVolumes

    fn.restype = ctypes.c_int

    fn.argtypes = [ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_char_p]

    caminho_arquivo = ctypes.c_char_p(bytes(caminho_arquivo, "utf-8"))
    qVol            = ctypes.c_char_p(bytes(qVol,            "utf-8"))
    especie         = ctypes.c_char_p(bytes(especie,         "utf-8"))
    marca           = ctypes.c_char_p(bytes(marca,           "utf-8"))
    nVol            = ctypes.c_char_p(bytes(nVol,            "utf-8"))
    pesoL           = ctypes.c_char_p(bytes(pesoL,           "utf-8"))
    pesoB           = ctypes.c_char_p(bytes(pesoB,           "utf-8"))

    return fn(caminho_arquivo, qVol, especie, marca, nVol, pesoL, pesoB)

def InformaLacres(caminho_arquivo, numero, indexVolume):
    
    fn = ffi.InformaLacres

    fn.restype = ctypes.c_int

    fn.argtypes = [ctypes.c_char_p,
    ctypes.c_char_p,
    ctypes.c_int]

    caminho_arquivo = ctypes.c_char_p(bytes(caminho_arquivo, "utf-8"))
    numero          = ctypes.c_char_p(bytes(numero,          "utf-8"))

    return fn(caminho_arquivo, numero, indexVolume)

def EmitirNota(caminho_arquivo, resultado_processamento):
    fn = ffi.EmitirNota
    fn.restype = ctypes.c_char_p
    fn.argtypes = [ctypes.c_char_p, ctypes.POINTER(ctypes.c_int)]

    caminho_arquivo = ctypes.c_char_p(bytes(caminho_arquivo, "utf-8"))

    return fn(caminho_arquivo, resultado_processamento).decode("utf-8")

def DecodificarNota(caminho_saida, dados, retorno):
    
    fn = ffi.DecodificarNota

    fn.restype = ctypes.c_char_p
    fn.argtypes = [ctypes.c_char_p, ctypes.c_char_p, ctypes.POINTER(ctypes.c_int)]

    caminho_saida = ctypes.c_char_p(bytes(caminho_saida, "utf-8"))
    dados         = ctypes.c_char_p(bytes(dados,         "utf-8"))

    return fn(caminho_saida, dados, retorno).decode("utf-8")


def ConsultarNota(chave, ret):
    
    fn = ffi.ConsultarNota

    fn.restype = ctypes.c_char_p
    fn.argtypes = [ctypes.c_char_p, ctypes.POINTER(ctypes.c_int)]

    chave = ctypes.c_char_p(bytes(chave, "utf-8"))

    return fn(chave, ret).decode("utf-8")


def CancelarNota(chave_nota, protocolo, justificativa, ret):
    fn = ffi.CancelarNota

    fn.restype = ctypes.c_char_p
    fn.argtypes = [ctypes.c_char_p, ctypes.c_char_p, ctypes.c_char_p, ctypes.POINTER(ctypes.c_int)]

    chave_nota      = ctypes.c_char_p(bytes(chave_nota,   "utf-8"))
    protocolo       = ctypes.c_char_p(bytes(protocolo,    "utf-8"))
    justificativa   = ctypes.c_char_p(bytes(justificativa,"utf-8"))

    return fn(chave_nota, protocolo, justificativa, ret).decode("utf-8")