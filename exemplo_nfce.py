import ctypes
import E1_NFCe

PATH_FILE = "./venda.json"

ret = 0

ret = E1_NFCe.AbreCupomVenda(PATH_FILE,     # caminho arquivo
                             "AM",          #UF
                             "",            #cNF
                             "venda",       #natOp
                             0,             #mod
                             "",            #serie
                             "",            #nNF
                             "",            #dhEmi
                             "",            #dhSaiEnt
                             0,             #tpNF
                             0,             #idDest
                             "",            #cMunFG
                             4,             #tpImp
                             0,             #tpEmis
                             0,             #cDV
                             0,             #tpAmb
                             0,             #finNFe
                             0,             #indFinal
                             1,             #indPres
                             0,             #indIntermed
                             0,             #procEmi
                             "",            #verProc
                             "",            #Data e Hora da entrada em contingência
                             "",            #Justificativa da entrada em contingência
                             "")            #Id para consultas futuras em caso de queda de conexão

print("Retorno AbreCupomVenda: ", ret)

ret = E1_NFCe.InformaEmitente(PATH_FILE,    # caminho arquivo
                          "14200166000166", # CNPJ
                          "",               # CPF
                          "",               # Razão social
                          "",               # nome fantasia
                          "",               # logradouro
                          "",               # número
                          "",               # complemento
                          "",               # bairro
                          0,                # código do municipio
                          "",               # nome municio
                          "",               # sigla uf
                          "",               # CEP
                          0,                # codigo pais
                          "",               # nome do pais
                          "",               # telefone
                          "",               # IE
                          "",               # IEST
                          "",               # IM
                          "",               # CNAE
                          0)                # CRT

print("Retorno informaEmitente: ", ret)

ret = E1_NFCe.InformaProduto(PATH_FILE,                 # caminho arquivo
                         "123",                 #cProd
                         "SEM GTIN",            #cEAN
                         "NFCe - Homologacao",  #xProd
                         "22030000",            #NCM
                         "",                    #NVE
                         "0302100",             #CEST
                         "S",                   #indEscala
                         "",                    #CNPJFab
                         "",                    #cBenef
                         "",                    #EXTIPI
                         "5102",                #CFOP
                         "UN",                  #uCom
                         "1.0000",              #qCom
                         "1.00",                #vUnCom
                         "",                    #vProd
                         "5000000000357",       #cEANTrib
                         "UN",                  #uTrib
                         "1.0000",              #qTrib
                         "1.00",                #vUnTrib
                         "",                    #vFrete
                         "",                    #vSeg
                         "",                    #vDesc
                         "",                    #vOutro
                         0)                     #indtot

print("Retorno informaProduto: ", ret)

ret = E1_NFCe.InformaICMS40(PATH_FILE,      # caminho do arquivo
                            1,              # numero do produto
                            0,              # orig
                            "41",           # CST
                            "",             # vICMSDeson
                            "")             # motDesICMS

print("Retorno informaICMS40: ", ret)

ret = E1_NFCe.InformaPISAliq(PATH_FILE,     # caminho arquivo
                            1,              #nItem
                            "01",           #CST
                            "1.00",         #vBC
                            "1.65",         #pPIS
                            "0.02")         #vPIS

print("Retorno informaPISAliq: ", ret)

ret = E1_NFCe.InformaCofinsAliq(PATH_FILE,  # caminho arquivo
                            1,              #nItem
                            "01",           #CST
                            "1.00",         #vBC
                            "7.60",         #pCofins
                            "0.08")         #vCofins

print("Retorno informaCofinsAliq: ", ret)

ret = E1_NFCe.InformaPagamento(PATH_FILE,    # caminho arquivo
                        0,                   #Indicador da Forma de pagamento
                        1,                   #Meio de pagamento
                        "1.00",              #valor pago
                        1,                   #Tipo integração
                        "",                  #cnpj
                        1,                   #bandeira
                        "",                  #codigo aut
                        "0.00")              #valor troco

print("Retorno informaPagamento: ", ret)

ret = E1_NFCe.InformaTransporte(PATH_FILE,      # caminho arquivo
                        9,                      # modalidade do frete
                        "",                     # CNPJ Transportadora
                        "",                     # CPF Transportadora
                        "",                     # Nome ou razão social
                        "",                     # IE
                        "",                     # endereço completo
                        "",                     # nome do municipio
                        "")                     # UF

print("Retorno informaTransporte: ", ret)

ret = E1_NFCe.InformaVolumes(PATH_FILE,          # caminho arquivo
                            "12",                # Quantidade de volumes transportados
                            "VOL",               # Espécie dos volumes transportados
                            "Elgin SA",          # Marca dos volumes transportados
                            "0 A 0",             # Marca dos volumes transportados
                            "20.123",            # Peso Líquido (em kg)   
                            "30.123")            # Peso Bruto (em kg)

print("Retorno informaVolumes: ", ret)

ret = E1_NFCe.InformaLacres(PATH_FILE,          # caminho arquivo
                            "3000",             # identificação do lacre
                            0)                  # identificação do volume retornado na função informa volumes

print("Retorno informaLacres: ", ret)

aux = ctypes.c_int()
result = E1_NFCe.EmitirNota(PATH_FILE, aux)

print("Resultado da emissão:", aux)
print("Retorno: ", result)

result = E1_NFCe.DecodificarNota("./", result, aux)
print("Resultado da decodificação:", aux)
print("Retorno: ", result)

