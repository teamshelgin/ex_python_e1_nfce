# Módulo NFCe - E1

Esse módulo tem como objetivo simplificar o processo de integração dos pontos de venda (PDV) com o ambiente SEFAZ de emissão de nota fiscal do consumidor (NFCe).
A aplicação de PDV deverá utilizar o módulo E1_NFCe para gerar os arquivos que serão transmitidos para sefaz e ao fim da geração devera enviar o arquivo gerado para função de emissão.
Antes de entrar nos detalhes de integração se faz necessários alguns esclarecimentos sobre esse módulo. Abaixo estão detalhados os componentes desse módulo

## Componentes

 **E1_NFCe(DLL/SO)** - Extensão de aplicativo (DLL/SO) onde estão exportadas todas as funções para criação do documento, emissão, consulta e cancelamento de notas. É por meio dessa que o PDV fara toda a integração com o sistema de NFCe.

 **api_settings.ini** - Arquivo de configuração da DLL, onde estão as configurações de timeout, identificação do PDV, URL da plataforma cloud e configurações de proxy.
 
 **NFCe Veraciti** - Plataforma cloud responsavel por realizar a assinatura e transmissão para a SEFAZ. Nesse sistema o parceiro irá criar toda sua estrutura, desde a empresa, PDVs, configuração dos certificados. Também será nessa plataforma onde o cliente poderá consultar as notas emitidas, status das notas, valores processados, etc.
 

## Por onde começar
### Cloud
- A integração com o sistema de emissão de nota fiscal do consumidor pode ser iniciado criando uma conta na plataforma veraciti. Caso deseje criar uma conta, solicite ao nosso canal de suporte ao desenvolvedor (https://elginbematech.com.br/chamado/).
- Com a conta criada, acesse https://nfce.veraciti.com.br/#/ e realize o login.
- Ao realizar o primeiro acesso, cadastre sua empresa(filial) no menu lateral Empresas. `Para emissão de NFCe será necessário configurar o certificado digital para assinatura dos documentos.`
- Em seguida, crie um PDV e selecione o tipo NFC-e. O PDV deve ser criado clicando sobre `Novo ponto de venda` na opções `PDV` da empresa criada anteriormente.
- Ao Criar o PDV, será gerado os dados de autenticação com a plataforma, ClientID e Client Secret. Guarde essas informações para configurar no arquivo api_settings.ini

### Local
- O primeiro passo será configurar no arquivo api_settings.ini os dados do PDV que está sendo configurado. Por padrão a DLL irá criar o arquivo caso no encontre o mesmo na pasta de trabalho.
Seu conteúdo padrão deve ser parecido com o descrito abaixo:

	   ``` ini
	   # Endereços da plataforma NFCE Veraciti
	   [ENDPOINTS]
	   raiz=https://console.veraciti.com.br/api/v1/
	   token=auth2_token/token
	   emissao=nfce/
	   consulta=nfce/{nfe_key}
	   cancelamento=nfce/{nfe_key}/cancel

	   # Configurações de proxy
	   [CONFIGURACOES_PROXY]
	   # Aceita 0 para desativar e 1 para ativar
	   ativado=0
	   endereco_ip=127.0.0.1
	   porta=8080
	   usuario=null
	   senha=null

	   # Dados de autenticação com plataforma Veraciti
	   [DADOS_CLIENTE]
	   # Substituir por dados gerados na criação do PDV
	   client_id=null
	   client_secret=null
	   timeout_comunicacao=30
	   ```

- Localmente, o PDV deve carregar a DLL/SO e realizar as chamadas referente a criação do documento de acordo com a venda e então chamar a função de Emissão da nota. 
- Ao fim desse fluxo o PDV irá receber um json com os detalhes da emissão e o arquivo de XML processado pela sefaz em BASE64.
- O PDV pode ainda usar a função de decodificação da nota para traduzir o BASE64 e gerar o arquivo em uma estrutura de pasta organizadas por data.

O fluxo de integração com o a DLL segue os seguintes passos:


![fluxo basico](https://bitbucket.org/teamshelgin/ex_python_e1_nfce/raw/2673a2e896111ea03fcfd8f2f90e6a79e76bae1f/doc/fluxo_basico_integracao.jpg)
